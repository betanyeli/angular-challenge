import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http/';
import { FirstRep } from '../interfaces/first-report.interface';

@Injectable({
  providedIn: 'root'
})
export class FirstReportService {
  info: FirstRep = {};
  endpoint = 'http://168.232.165.184/prueba/array';
  constructor( private http: HttpClient) {

this.generateReport();

   }
   /* Suscribe method... return an "Observable", that's content the api response */
   private generateReport() {
    this.http.get(this.endpoint)
    .subscribe( (res: FirstRep) => {
      if (res.data !== null) {
        this.info.data = res.data;
        console.log('Service response => ', res.data);
      } else {
        alert('This is not working, dude');
        console.log('Error service =>', res.data);
      }
    });
   }



  }
