import { Component, OnInit } from '@angular/core';
import { FirstReportService } from '../../services/first-report.service';
import { HttpClient } from '@angular/common/http/';
import { FirstRep } from '../../interfaces/first-report.interface';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  endpoint = 'http://168.232.165.184/prueba/array'; // initializing endpoint
  constructor(public service: FirstReportService, private http: HttpClient) {}
  generateReportButton() {
    this.http.get(this.endpoint).subscribe((response: FirstRep) => {
      if (response.data !== null) {
        alert('Reload to view changes');
        console.log('response', response);
      } else {
        alert('Keep trying...');
        console.log('response', response);
      }
    });
  } // end button

  ngOnInit() {}
}
