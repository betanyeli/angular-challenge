# Angular Challenge for Page Personnel.

**To-do List.**

------------
- Learn Angular 8,Typescript, MVC Models, with an Udemy resource and Angular docs: [View here](https://www.udemy.com/course/html-hacia-angular/learn/lecture/7699236?start=450#overviewhttp:// "View here")
- Create a boilerplate
	- Install Bootstrap dependencies.
	- Create views and components.
- Create a Service, and Interfaces of response api.
- Create a HTTPClientModule, to get a response api.
- Fill table dynamically with response api.
	- Use NgDirectives (For, If)

Resources used:
- Angular 8.
- Typescript.
- JavaScript ES6.
- Bootstrap library.
- Firebase dependencies.

The technical challenge is incomplete, however, I learned in three days about the structure of **AngularJs (MVC Model) **compared to **ReactJs (Flux Model)**.

> Powered by Betányeli Bravo.